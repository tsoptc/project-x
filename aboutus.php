<!DOCTYPE html>
<html>
<head>
	<title>About Us</title>
	<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

	<?php
	include "header.php";
	?>

	<nav>
		<div class="nav-wrapper grey darken-4">
			<div class="col s12">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="#!" class="breadcrumb">About Us</a>
			</div>
		</div>
	</nav>

	<h6 class="aboutusheader">About Us</h6>

	<div class="row">
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s3"><a href="#test1">About Us</a></li>
				<li class="tab col s3"><a href="#test2">Mindaugas</a></li>
				<li class="tab col s3"><a href="#test3">Andrius</a></li>
				<li class="tab col s3"><a href="#test4">Viktoras</a></li>
			</ul>
		</div>
		<div id="test1" class="col s12">
			<img class="aboutimages" src="images/projectxlogo.png">
			<h5>Project-X</h5>
			<p>We are Project-X -  a hardcore punk band formed in Vilnius, Lithuania in 2013.
				After a few demos and an EP, we signed to Burning Heart Records in 2014, and released our first album, <i>The Beginning</i>, on the 14th of April, 2014.
				We followed this with our second album <i>The Deadly Rhythm</i>, released the following year on the 20th of May, again via Burning Heart Records, which saw us move in a heavier direction and was very well received.  
				After the success of <i>The Deadly Rhythm</i> we signed to Burning Heart's parent label, Epitapth Records, and released our third and most recent album, <i>War on the Palaces</i>, on the 13th of November, 2017.
			We are currently on tour, and will begin working on the next album shortly after we're done. </p>
			<p>Subscribe to our newsletter to get all the updates!</p>
		</div>
		<div id="test2" class="col s12">
			<img class="aboutimages z-depth-2 materialboxed" src="images/microphone.jpg">
			<h5>Mindaugas - Vocals</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			<a class="waves-effect waves-light btn grey darken-4" href="mindaugas.php">More About Mindaugas</a>
		</div>
		<div id="test3" class="col s12">
			<img class="aboutimages z-depth-2 materialboxed" src="images/guitar.jpg">
			<h5>Andrius - Guitar</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			<a class="waves-effect waves-light btn grey darken-4" href="andrius.php">More About Andrius</a>
		</div>
		<div id="test4" class="col s12">
			<img class="aboutimages z-depth-2 materialboxed" src="images/drums.jpg">
			<h5>Viktoras - Drums</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			<a class="waves-effect waves-light btn grey darken-4" href="viktoras.php">More About Viktoras</a>
		</div>
	</div>
</div>

<hr>

<div class="aboutusbottom">
	<div class="map">
		<iframe class="z-depth-2" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2304.9348204421262!2d25.291859215877405!3d54.710769880287586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd96b1c1c3bc9f%3A0xb42a2064e782dc0c!2sJ.+Galvyd%C5%BEio+g.+5%2C+Vilnius+08236!5e0!3m2!1sen!2slt!4v1531157794694" width="500" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="contactinfotable">			
		<table class="centered highlight responsive-table">
			<thead>
				<tr>
					<th>Project-X</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Address</td>
					<td>J. Galvydžio g. 5</td>
				</tr>
				<tr>
					<td>City</td>
					<td>Vilnius</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>projectxband@gmail.com</td>
				</tr>
				<tr>
					<td>Phone</td>
					<td>666 666 666</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<hr>

<?php
include "footer.php";
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

<script type="text/javascript" src="Scripts/script.js"></script>
</body>
</html>