<!DOCTYPE html>
<html>
<head>
	<title>Andrius</title>
	<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/style.css">

	<link rel="stylesheet" type="text/css" href="Styles/viktoras.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>

	<?php
	include "header.php";
	?>


	<nav>
		<div class="nav-wrapper grey darken-4">
			<div class="col s12">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="aboutus.php" class="breadcrumb">About Us</a>
				<a href="#!" class="breadcrumb">Andrius</a>
			</div>
		</div>
	</nav>

	<div class="container">

		<h4 class="aboutusheader">Andrius</h4>

		<div class="memberinfo">
			<img class="instrument" src="images/guitar.jpg">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		</div>

	</div>

	<hr>

	<div class="carousel">
		<a class="carousel-item" href="#one!"><img src="images/drums.jpg"></a>
		<a class="carousel-item" href="#two!"><img src="images/guitar.jpg"></a>
		<a class="carousel-item" href="#three!"><img src="images/microphone.jpg"></a>
	</div>

	<div class="parallax-container p-c2">
		<div class="parallax">
			<img class="parallax-instruments" src="images/guitar1.png">
		</div>
	</div>

	<hr>

	<div class="container">
		<h4 class="aboutusheader">Other Members</h4>

		<div class="row">
			<div class="col s12 m6">
				<img src="images/microphone.jpg">
				<h6>Mindaugas - vocals</h6>
				<a class="waves-effect waves-light btn-small grey darken-4" href="mindaugas.php">More</a>
			</div>
			<div class="col s12 m6">
				<img src="images/drums2.jpg">
				<h6>Viktoras - drums</h6>
				<a class="waves-effect waves-light btn-small grey darken-4" href="viktoras.php">More</a>
			</div>
		</div>

	</div>

	<?php
	include "footer.php";
	?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>

</body>

</html>