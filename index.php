<!DOCTYPE html>
<html>
<head>
	<title>Project X</title>
	<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

	<?php
	include "header.php";
	?>

  <nav>
    <div class="nav-wrapper grey darken-4">
      <div class="col s12">
        <a href="#!" class="breadcrumb">Index</a>
      </div>
    </div>
  </nav>

  <div id="modal1" class="modal indexmodal">
    <div class="modal-content">
      <h4>Project-X</h4>
      <p>We are Project-X - a hardcore punk band formed in Vilnius, Lithuania in 2013. After a few demos and an EP, we signed to Burning Heart Records in 2014, and released our first album, The Beginning, on the 14th of April, 2014. We followed this with our second album The Deadly Rhythm, released the following year on the 20th of May, again via Burning Heart Records, which saw us move in a heavier direction and was very well received. After the success of The Deadly Rhythm we signed to Burning Heart's parent label, Epitapth Records, and released our third and most recent album, War on the Palaces, on the 13th of November, 2017. We are currently on tour, and will begin working on the next album shortly after we're done. </p>

      <a class="waves-effect waves-light btn-large tooltipped grey darken-4" data-position="bottom" data-tooltip="Read more about us!" href="aboutus.php">More Info</a>
    </div>
  </div>

	<div class="main-page">

		<br>

		<p class="flow-text"><b>Project-X was formed in 2013 in Vilnius, Lithuania, by three programmers.</b></p>

		<br>

		<a onclick="M.toast({html: 'We are on tour! <br> Come see us live!'})" class="waves-effect waves-light btn-large modal-trigger grey darken-4" href="#modal1">About Project-X</a>

		<br>

		<p class="flow-text"><b>We aim to deliver an old-school European hardcore punk experience while incorporating modern touches and elements of other genres to bring our sound up to date. </b></p>
		<p class="flow-text"><b>Our members are influenced by a variety of genres, from post-hardcore, sludge and post-metal, through black, progressive, and avant-garde metal, to garage and blues rock, and we combine those influences for our unique Project-X sound.</b></p>

		<p class="flow-text"><b>We are currently touring across Europe and will be playing in most major cities throughout the rest of the year, so check the dates and come out to see us. Once we wrap the tour up, we're planning on heading straight into VCS Studios to finish writing and record our fourth album, details about which will be revealed soon. Sign up for the newsletter so you don't miss it.</b></p>

		<br>

	</div>

	<hr>

	<ul class="albumlist">
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 01.jpg">
				<p>The Beginning</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album1.php">More Info</a>
				<!-- <p>Album 3 Album 3 Album 3 Album 3 Album 3 Album 3</p> -->
				<p>Released 14 April 2014, via Burning Heart Records</p>
			</li>
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 02.jpg">
				<p>The Deadly Rhythm</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album2.php">More Info</a>
				<!-- <p>Album 3 Album 3 Album 3 Album 3 Album 3 Album 3</p> -->
				<p>Released 20 May 2016, via Burning Heart Records</p>
			</li>
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 03.jpg">
				<p>War on the Palaces</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album3.php">More Info</a>
				<!-- <p>Album 3 Album 3 Album 3 Album 3 Album 3 Album 3</p> -->
				<p>Released 13 November 2017, via Epitaph Records</p>
			</li>
	</ul>

	<?php
	include "footer.php";
	?>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>
</body>
</html>