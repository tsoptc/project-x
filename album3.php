<!DOCTYPE html>
<html>
<head>

	<title>Project X</title>
		<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/albums.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>
<body>

	<?php
	include "header.php";

	?>

	<nav>
		<div class="nav-wrapper">
			<div class="col s12 grey darken-4">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="albums.php" class="breadcrumb">Albums</a>
				<a href="#!" class="breadcrumb">War on the Palaces</a>
			</div>
		</div>
	</nav>

	<div class="album 3"> 

		<h2>War on the Palaces</h2>

		<div class="container">
			<div class="row gridasMain">
				<div class="col s12"><img class="z-depth-2" src="images/Album 03.jpg">
					<ul class="collapsible">
					<li>
						<div class="collapsible-header"><i class="material-icons">audiotrack</i>Tracks</div>
						<div class="collapsible-body"><span><p>01. War on the Palaces</p><p>02. Coup d’état</p><p>03. Ascension</p><p>04. City of Death</p><p>05. Hand of Stone</p><p>06. Oblivion</p><p>07. Buried by Time and Dust</p><p>08. Glimpse of the Unseen</p><p>09. 366</p><p>10. Liberation Frequency</p><p>11. Phoenix</p><p>12. Consequence</p><p>13. For This We Fought the Battle of Ages</p><p>14. Seven Nation Army</p></span></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="material-icons">date_range</i>Release date</div>
						<div class="collapsible-body"><span>13 November 2017
						</span></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="material-icons">business_center</i>Record Label</div>
						<div class="collapsible-body"><span>Burning Heart Records</span></div>
					</li>
				</ul>
			</div>
		</div>

		<ul class="albumlist">
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 01.jpg">
				<p>The Beginning</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album1.php">More</a>
				<p>Released 14 April 2014, via Burning Heart Records</p>
			</li>
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 02.jpg">
				<p>The Deadly Rhythm</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album2.php">More</a>
				<p>Released 20 May 2016, via Burning Heart Records</p>
			</li>
	</ul>

	</div>


	<?php
	include "footer.php";
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>
</body>
</html>