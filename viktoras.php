<!DOCTYPE html>
<html>
<head>
	<title>Viktoras</title>
	<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/style.css">

	<link rel="stylesheet" type="text/css" href="Styles/viktoras.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>

	<?php
	include "header.php";
	?>


	<nav>
		<div class="nav-wrapper grey darken-4">
			<div class="col s12">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="aboutus.php" class="breadcrumb">About Us</a>
				<a href="#!" class="breadcrumb">Viktoras</a>
			</div>
		</div>
	</nav>

	<div class="container">

		<h4 class="aboutusheader">Viktoras</h4>

		<div class="memberinfo">
			<img class="instrument" src="images/drums.jpg">
			<p>Viktoras aka Flying Whale (born December 26, 1975) is a Lithuanian musician, songwriter. He is best known as the drummer and co-founder of the Lithuanian hardcore punk band Project-X, which was started in VCS studio. Viktoras started to study music at age 5, first as a piano player and then moved to play drums at age 14. After that he was playing in many different styles of musical projects such as percussion music, rock/metal music, classical music, jazz, world music etc.</p>
		</div>

	</div>

	<hr>

	<div class="carousel">
		<a class="carousel-item" href="#one!"><img src="images/drums.jpg"></a>
		<a class="carousel-item" href="#two!"><img src="images/guitar.jpg"></a>
		<a class="carousel-item" href="#three!"><img src="images/microphone.jpg"></a>
	</div>

	<div class="parallax-container p-c1">
		<div class="parallax">
			<img class="parallax-instruments" src="images/drums1.jpg">
		</div>
	</div>

	<hr>

	<div class="container">
		<h4 class="aboutusheader">Other Members</h4>
		<div class="row">
			<div class="col s12 m6">
				<img src="images/microphone.jpg">
				<h6>Mindaugas - vocals</h6>
				<a class="waves-effect waves-light btn-small grey darken-4" href="mindaugas.php">More</a>
			</div>
			<div class="col s12 m6">
				<img src="images/guitar.jpg">
				<h6>Andrius - guitars</h6>
				<a class="waves-effect waves-light btn-small grey darken-4" href="andrius.php">More</a>
			</div>
		</div>
	</div>

	<?php
	include "footer.php";
	?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>

</body>

</html>