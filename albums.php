<!DOCTYPE html>
<html>
<head>

	<title>Project X</title>
		<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/albums.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>
<body>

	<?php
	include "header.php";

	?>

	<nav>
		<div class="nav-wrapper">
			<div class="col s12 grey darken-4">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="#!" class="breadcrumb">Albums</a>
			</div>
		</div>
	</nav>

	<div class="albums"> 

		<h2>Albums</h2>



		<div class="container">
			<div class="row gridas">
				<div class="col s12 m4 l2"><img class="z-depth-2" src="images/Album 01.jpg"></div>
				<div class="col s12 m4 l8"><a class="waves-effect waves-white btn-flat btn-medium mygtukasTitle" href="album1.php"><h4>The Beginning</h4></a><p class="aprasymas">Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1 Album 1Album 1 Album 1 Album 1 Album 1 Album 1 Album 1</p></div>
				<div class="col s12 m4 l2"><a class="waves-effect waves-light btn-large grey darken-4 mygtukas" href="album1.php">More</a></div>
						
				
				
			</div>

			<div class="row gridas">
				<div class="col s12 m4 l2"><img class="z-depth-2" src="images/Album 02.jpg"></div>
				<div class="col s12 m4 l8"><a class="waves-effect waves-white btn-flat btn-medium mygtukasTitle" href="album2.php"><h4>The Deadly Rhythm</h4></a><p class="aprasymas">Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 Album 2 </p></div>
				<div class="col s12 m4 l2"><a class="waves-effect waves-light btn-large grey darken-4 mygtukas" href="album2.php">More</a></div>
						
				
				
			</div>

			<div class="row gridas">
				<div class="col s12 m4 l2"><img class="z-depth-2" src="images/Album 03.jpg"></div>
				<div class="col s12 m4 l8"><a class="waves-effect waves-white btn-flat btn-medium mygtukasTitle" href="album3.php"><h4>War on the Palaces</h4></a><p class="aprasymas">Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 Album 3 </p></div>
				<div class="col s12 m4 l2"><a class="waves-effect waves-light btn-large grey darken-4 mygtukas"href="album3.php">More</a></div>
						
				
				
			</div>
		</div>

	</div>


	<?php
	include "footer.php";
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>
</body>
</html>