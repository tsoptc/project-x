<!DOCTYPE html>
<html>
<head>
	<title>Subscribers</title>
	<meta charset="utf-8">
</head>
<body>

	<?php
		include "db.php";

		echo "Connected successfully";
		echo "<br><br>";

		mysqli_set_charset($conn, "utf8");


		$sql = "SELECT id, name, email FROM Subscribers";
		$result = mysqli_query($conn, $sql);


		if (mysqli_num_rows($result) > 0) {

			echo "<table border='1'>";

		    // output data of each row
		    while($row = mysqli_fetch_assoc($result)) {

		    	echo "<tr>";

			        // print_r($row);
			        // echo $row["vardas"];
			        // echo "<br>";

		    		echo "<td>" . $row["id"] . "</td>";
		    		echo "<td>" . $row["name"] . "</td>";
		    		echo "<td>" . $row["email"] . "</td>";
		    		
		        echo "</tr>";
		    }

		    echo "</table>";

		} else {

		    echo "0 results";
		}

		mysqli_close($conn);

	?>