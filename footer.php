        <footer class="page-footer grey darken-4">
          <div class="footer-copyright">
            <div class="container">
                © 2018 Project-X
                <div class="footerimages">
                    <a href="https://www.facebook.com/RefusedBand/" target="blank"><img src="images/facebook.png"></a>
                    <a href="https://twitter.com/refused?lang=en" target="blank"><img src="images/twitter.png"></a>
                    <a href="https://www.youtube.com/watch?v=l5a1xuO4LaY" target="blank"><img src="images/youtube.png"></a>
                </div>
                <a class="grey-text text-lighten-4 right" href="aboutus.php">About Us</a>
                <br>
                <a class="grey-text text-lighten-4 right modal-trigger" href="#modal2">Newsletter</a>
            </div>
        </div>
    </footer>

    <div id="modal2" class="modal indexmodal">
        <div class="modal-content">
          <h4>Subscribe to our newsletter!</h4>
          <p>Subscribe to receive news about our tours, new music, and our other projects so that you don't miss anything.</p>

          <?php
          include "db.php";

          if (isset($_GET["name"])) {

              if ($_GET["name"] != '' && $_GET["email"] != '') {

                $sql = "INSERT INTO subscribers (id, name, email)
                VALUES (null, '" . $_GET["name"] . "', '" . $_GET["email"] . "')";

                if (mysqli_query($conn, $sql)) {
                    echo "Subscribed!";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }
        }
        mysqli_close($conn);
        ?>

        <form>
            <p>Name:</p>
            <input type="text" name="name">
            <p>Email address:</p>
            <input type="text" name="email">
            <input type='submit' class="waves-effect waves-light btn-large grey darken-4">
        </form>


    </div>
