<!DOCTYPE html>
<html>
<head>

	<title>Project X</title>
	<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/albums.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>
<body>

	<?php
	include "header.php";

	?>

	<nav>
		<div class="nav-wrapper">
			<div class="col s12 grey darken-4">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="albums.php" class="breadcrumb">Albums</a>
				<a href="#!" class="breadcrumb">The Beginning</a>
			</div>
		</div>
	</nav>

	<div class="album 1"> 

		<h2>The Beginning</h2>

		<div class="container">
			<div class="row gridasMain">
				<div class="col s12"><img class="z-depth-2" src="images/Album 01.jpg">

				<ul class="collapsible">
					<li>
						<div class="collapsible-header"><i class="material-icons">audiotrack</i>Tracks</div>
						<div class="collapsible-body"><span><p>01. Initiating...</p><p>02. Project-X</p><p>03. Symbols</p><p>04. New Noise</p><p>05. Waiting for Salvation</p><p>06. Black Mask</p><p>07. Interlude</p><p>08. A New Language</p><p>09. The Transmission</p><p>10. The Subversive Sound</p><p>11. Up for Sale</p></span></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="material-icons">date_range</i>Release date</div>
						<div class="collapsible-body"><span>14 April 2014
						</span></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="material-icons">business_center</i>Record Label</div>
						<div class="collapsible-body"><span>Burning Heart Records</span></div>
					</li>
				</ul>

			</div>
		</div>

		<ul class="albumlist">
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 02.jpg">
				<p>The Deadly Rhythm</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album2.php">More</a>
				<p>Released 20 May 2016, via Burning Heart Records</p>
			</li>
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 03.jpg">
				<p>War on the Palaces</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album3.php">More</a>
				<p>Released 13 November 2017, via Epitaph Records</p>
			</li>
		</ul>

	</div>


	<?php
	include "footer.php";
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>
</body>
</html>