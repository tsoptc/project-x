<!DOCTYPE html>
<html>
<head>

	<title>Project X</title>
		<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="Styles/albums.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>
<body>

	<?php
	include "header.php";

	?>

	<nav>
		<div class="nav-wrapper">
			<div class="col s12 grey darken-4">
				<a href="index.php" class="breadcrumb">Index</a>
				<a href="albums.php" class="breadcrumb">Albums</a>
				<a href="#!" class="breadcrumb">The Deadly Rhythm</a>
			</div>
		</div>
	</nav>

	<div class="album 2"> 

		<h2>The Deadly Rhythm</h2>

		<div class="container">
			<div class="row gridasMain">
				<div class="col s12"><img class="z-depth-2" src="images/Album 02.jpg">
					<ul class="collapsible">
					<li>
						<div class="collapsible-header"><i class="material-icons">audiotrack</i>Tracks</div>
						<div class="collapsible-body"><span><p>01. Intro</p><p>02. The Deadly Rhythm</p><p>03. Pressure</p><p>04. Pulse</p><p>05. Recollection</p><p>06. This Constant War</p><p>07. Down in the Shadows</p><p>08. Revolution</p><p>09. Shake the Cage</p><p>10. Thorn</p><p>11. Illuminate</p><p>12. Resistance</p><p>13. Outro</p></span></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="material-icons">date_range</i>Release date</div>
						<div class="collapsible-body"><span>20 May 2016
						</span></div>
					</li>
					<li>
						<div class="collapsible-header"><i class="material-icons">business_center</i>Record Label</div>
						<div class="collapsible-body"><span>Burning Heart Records</span></div>
					</li>
				</ul>
			</div>
		</div>

		<ul class="albumlist">
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 01.jpg">
				<p>The Beginning</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album1.php">More</a>
				<p>Released 14 April 2014, via Burning Heart Records</p>
			</li>
			<li class="albumlist">
				<img class="z-depth-2" src="images/Album 03.jpg">
				<p>War on the Palaces</p>
				<a class="waves-effect waves-light btn-small grey darken-4" href="album3.php">More</a>
				<p>Released 13 November 2017, via Epitaph Records</p>
			</li>
	</ul>

	</div>


	<?php
	include "footer.php";
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script type="text/javascript" src="Scripts/script.js"></script>
</body>
</html>